# create 5 variables
name = "jose"
age = 38
occupation = "writer"
movie = "One more chance"
rating = 99.96

# output the variables
print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%")

# create 3 variables and perform operations
num1 = 10
num2 = 15
num3 = 30

# get the product of num1 and num2
product = num1 * num2
print(product)

# check if num1 is less than num3
comparison = num1 < num3
print(comparison)

# add the value of num3 to num2
num2 += num3
print(num2)
